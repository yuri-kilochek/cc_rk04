#!/usr/bin/env python

def push(stack):
    r = frozenset()
    stack.append(r)

def dup(stack):
    r = stack.pop()
    stack.append(r)
    stack.append(r)

def union(stack):
    a = stack.pop()
    b = stack.pop()
    r = a | b
    stack.append(r)

def intersect(stack):
    a = stack.pop()
    b = stack.pop()
    r = a & b
    stack.append(r)

def add(stack):
    a = stack.pop()
    b = stack.pop()
    r = b | frozenset({a})
    stack.append(r)

code = {
    'PUSH': push,
    'DUP': dup,
    'UNION': union,
    'INTERSECT': intersect,
    'ADD': add,
}

for p in range(int(input())):
    stack = []
    for i in range(int(input())):
        code[input()](stack)
        print(len(stack[-1]))
    print('***')

